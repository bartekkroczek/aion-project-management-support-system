SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `AIONdb` ;
CREATE SCHEMA IF NOT EXISTS `AIONdb` DEFAULT CHARACTER SET utf8 ;
USE `AIONdb` ;

-- -----------------------------------------------------
-- Table `AIONdb`.`GUI`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`GUI` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`GUI` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`Statistics`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`Statistics` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`Statistics` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`User` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`User` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Age` SMALLINT(6) NULL DEFAULT NULL ,
  `EmailAddress` VARCHAR(45) NOT NULL ,
  `HowManyAbandonedTasks` INT(11) NOT NULL ,
  `HowManyDoneTasks` INT(11) NOT NULL ,
  `LastLogged` DATE NULL DEFAULT NULL ,
  `Name` VARCHAR(45) NULL DEFAULT NULL ,
  `Password` VARCHAR(56) NOT NULL ,
  `PhoneNumber` VARCHAR(45) NULL DEFAULT NULL ,
  `Surname` VARCHAR(45) NULL DEFAULT NULL ,
  `Username` VARCHAR(45) NOT NULL ,
  `GUI_id` INT(11) NULL DEFAULT NULL ,
  `Statistics_id` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `UK_54ff6e555ee04d309d73e3277e5` (`EmailAddress` ASC) ,
  UNIQUE INDEX `UK_712e88f37c0e47c4a748b2fcab8` (`Username` ASC) ,
  INDEX `FK_62c3db8bf09c4e9cb6be717819a` (`GUI_id` ASC) ,
  INDEX `FK_58c21245c5994cfebd1be8f0a39` (`Statistics_id` ASC) ,
  CONSTRAINT `FK_58c21245c5994cfebd1be8f0a39`
    FOREIGN KEY (`Statistics_id` )
    REFERENCES `AIONdb`.`Statistics` (`id` ),
  CONSTRAINT `FK_62c3db8bf09c4e9cb6be717819a`
    FOREIGN KEY (`GUI_id` )
    REFERENCES `AIONdb`.`GUI` (`id` ))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`ProjectLeader`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`ProjectLeader` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`ProjectLeader` (
  `id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_0479601b39c640fda1b98ccf6c0` (`id` ASC) ,
  CONSTRAINT `FK_0479601b39c640fda1b98ccf6c0`
    FOREIGN KEY (`id` )
    REFERENCES `AIONdb`.`User` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`Project`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`Project` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`Project` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Description` LONGTEXT NULL DEFAULT NULL ,
  `FinishDate` DATE NULL DEFAULT NULL ,
  `HowManyTasks` INT(11) NOT NULL ,
  `PercentComplete` DOUBLE NOT NULL ,
  `ProjectName` VARCHAR(45) NOT NULL ,
  `StartDate` DATE NULL DEFAULT NULL ,
  `ProjectLeader_User_id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `UK_a702599047bc44419acdea6c144` (`ProjectName` ASC) ,
  INDEX `FK_2aeba832f4b04596a9089125e12` (`ProjectLeader_User_id` ASC) ,
  CONSTRAINT `FK_2aeba832f4b04596a9089125e12`
    FOREIGN KEY (`ProjectLeader_User_id` )
    REFERENCES `AIONdb`.`ProjectLeader` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`Milestone`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`Milestone` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`Milestone` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Description` LONGTEXT NULL DEFAULT NULL ,
  `Name` VARCHAR(45) NOT NULL ,
  `PercentComplete` DOUBLE NOT NULL ,
  `Project_id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_375d5b0ee9fc4096bfc119f0eff` (`Project_id` ASC) ,
  CONSTRAINT `FK_375d5b0ee9fc4096bfc119f0eff`
    FOREIGN KEY (`Project_id` )
    REFERENCES `AIONdb`.`Project` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`ITMilestone`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`ITMilestone` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`ITMilestone` (
  `id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_b8724db9cf964e2f84a11b2982f` (`id` ASC) ,
  CONSTRAINT `FK_b8724db9cf964e2f84a11b2982f`
    FOREIGN KEY (`id` )
    REFERENCES `AIONdb`.`Milestone` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`ITProject`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`ITProject` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`ITProject` (
  `id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_19807ed4f92646ebb00aebb01e4` (`id` ASC) ,
  CONSTRAINT `FK_19807ed4f92646ebb00aebb01e4`
    FOREIGN KEY (`id` )
    REFERENCES `AIONdb`.`Project` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`Status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`Status` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`Status` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Status` VARCHAR(255) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`Task`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`Task` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`Task` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Description` LONGTEXT NULL DEFAULT NULL ,
  `ExecutionDate` DATE NULL DEFAULT NULL ,
  `FinishDate` DATE NULL DEFAULT NULL ,
  `Name` VARCHAR(45) NOT NULL ,
  `StartDate` DATE NULL DEFAULT NULL ,
  `Tag` VARCHAR(255) NULL DEFAULT NULL ,
  `TakeDate` DATE NULL DEFAULT NULL ,
  `Milestone_id` INT(11) NOT NULL ,
  `Project_id` INT(11) NOT NULL ,
  `Status_id` INT(11) NOT NULL ,
  `User_id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_b90d1c36cc2b47869c97cf6b262` (`Milestone_id` ASC) ,
  INDEX `FK_f37f598a7a8f413ab67723a85bd` (`Project_id` ASC) ,
  INDEX `FK_2e6b16fe35fb4caea0395af9dfd` (`Status_id` ASC) ,
  INDEX `FK_2b5313b31486443cb5418e2d761` (`User_id` ASC) ,
  CONSTRAINT `FK_2b5313b31486443cb5418e2d761`
    FOREIGN KEY (`User_id` )
    REFERENCES `AIONdb`.`User` (`id` ),
  CONSTRAINT `FK_2e6b16fe35fb4caea0395af9dfd`
    FOREIGN KEY (`Status_id` )
    REFERENCES `AIONdb`.`Status` (`id` ),
  CONSTRAINT `FK_b90d1c36cc2b47869c97cf6b262`
    FOREIGN KEY (`Milestone_id` )
    REFERENCES `AIONdb`.`Milestone` (`id` ),
  CONSTRAINT `FK_f37f598a7a8f413ab67723a85bd`
    FOREIGN KEY (`Project_id` )
    REFERENCES `AIONdb`.`Project` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`ITTask`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`ITTask` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`ITTask` (
  `id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_6089f49ee0bc474fb2f358d48cf` (`id` ASC) ,
  CONSTRAINT `FK_6089f49ee0bc474fb2f358d48cf`
    FOREIGN KEY (`id` )
    REFERENCES `AIONdb`.`Task` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`LeaderGUI`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`LeaderGUI` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`LeaderGUI` (
  `id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_2ac607303aa34e599b28477a93e` (`id` ASC) ,
  CONSTRAINT `FK_2ac607303aa34e599b28477a93e`
    FOREIGN KEY (`id` )
    REFERENCES `AIONdb`.`GUI` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`LeaderStatistics`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`LeaderStatistics` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`LeaderStatistics` (
  `HowManyDeletedProjects` INT(11) NOT NULL ,
  `HowManyDoneProjects` INT(11) NOT NULL ,
  `HowManyProjects` INT(11) NOT NULL ,
  `id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_969254ee6f7d4bb097d00e29d0a` (`id` ASC) ,
  CONSTRAINT `FK_969254ee6f7d4bb097d00e29d0a`
    FOREIGN KEY (`id` )
    REFERENCES `AIONdb`.`Statistics` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`Programmer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`Programmer` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`Programmer` (
  `id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_354fecbd2cfb4b10ae19975a5a0` (`id` ASC) ,
  CONSTRAINT `FK_354fecbd2cfb4b10ae19975a5a0`
    FOREIGN KEY (`id` )
    REFERENCES `AIONdb`.`User` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`ProgrammerGUI`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`ProgrammerGUI` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`ProgrammerGUI` (
  `id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_544535f0a9fe44afaa1159a551c` (`id` ASC) ,
  CONSTRAINT `FK_544535f0a9fe44afaa1159a551c`
    FOREIGN KEY (`id` )
    REFERENCES `AIONdb`.`GUI` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`ProgrammerStatistics`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`ProgrammerStatistics` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`ProgrammerStatistics` (
  `id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_adf79cafe1e146ff8f9f089c65e` (`id` ASC) ,
  CONSTRAINT `FK_adf79cafe1e146ff8f9f089c65e`
    FOREIGN KEY (`id` )
    REFERENCES `AIONdb`.`Statistics` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `AIONdb`.`Task3Statuses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AIONdb`.`Task3Statuses` ;

CREATE  TABLE IF NOT EXISTS `AIONdb`.`Task3Statuses` (
  `id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_7a2b7360ea56499cb2709a19ccc` (`id` ASC) ,
  CONSTRAINT `FK_7a2b7360ea56499cb2709a19ccc`
    FOREIGN KEY (`id` )
    REFERENCES `AIONdb`.`Status` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
