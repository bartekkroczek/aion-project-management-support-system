package pojo;

import java.util.List;

import org.hibernate.Hibernate;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Przyk�ad zapisu do bazy danych za pomoc� metody saveOrUpdateObject
		// Tworz� nowego programist� i zapisuj� do bazy danych
		// To tak jakby rejestracja
//		Programmer programmer = new Programmer();
//		programmer.setUsername("JakisUser");
//		programmer.setPassword("dupa");
//		Database.saveOrUpdate(programmer);
//		System.out.println(programmer.getId()); // id zosta�o zaktualizowane z bazy danych
//		
//		// Tworz� lidera projektu
//		ProjectLeader projectLeader = new ProjectLeader();
//		projectLeader.setUsername("JakisLider");
//		projectLeader.setPassword("dupa2");
//		
//		// Tworz� nowy projekt
//		ITProject project = new ITProject();
//		project.setProjectName("JakisProjekt");
//		
//		// Tworz� nowy kamie� milowy
//		ITMilestone milestone = new ITMilestone();
//		milestone.setName("JakisMilestone");
//		
//		// Utworzony kamie� milowy nale�y projektu, dzia�a w jedn� stron�,
//		// ale dbajmy o prawid�owe powi�zania, to wa�ne
//		project.getMilestones().add(milestone);
//		milestone.setProject(project);
//		
//		// Tworz� nowy task
//		ITTask task = new ITTask();
//		task.setName("JakisTask");
//		Task4Statuses status = new Task4Statuses();
//		task.setStatus(status);
//		
//		// Utworzony task wchodzi w sk�ad kamienia milowego i projektu
//		milestone.getTasks().add(task);
//		task.setMilestone(milestone);
//		project.getTasks().add(task);
//		task.setProject(project);
//		
//		// Utworzony projekt nale�y lidera projektu
//		projectLeader.getProjects().add(project);
//		project.setProjectLeader(projectLeader);
//		
//		// Zapisuj� do bazy danych, teraz wystarczy zapisa� tylko lidera, w��czona kaskada
//		Database.saveOrUpdate(projectLeader);
		
		// Przyk�ad u�ycia getUser - pobiera u�ytkownika o podanym username i password
		// oraz sprawdza uprawnienia
		// Jesli jest programist� to sprawdzam, czy wzi�� jaki� task
		// Jesli jest liderem to dodatkowo wypisuj� jego projekty
//		User user = null;
//		try {
//			user = Database.getUser("JakisUser", "fuckyeah");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		if (user instanceof Programmer) {
//			Programmer p = (Programmer) user;
//			System.out.println("Jestem programist�! " + p.getUsername());
//			if (p.getTask() != null) {
//				System.out.println("Wzi��em task: " + p.getTask().getName());
//			}
//		} else {
//			ProjectLeader pl = (ProjectLeader) user;
//			System.out.println("Jestem liderem projektu! " + pl.getUsername());
//			for (Project project1 : pl.getProjects()) {
//				System.out.println(project1.getProjectName());
//			}
//		}
//		
//		try {
//			user = Database.getUser("JakisLider", "dupa2");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		if (user instanceof Programmer) {
//			Programmer p = (Programmer) user;
//			System.out.println("Jestem programist�! " + p.getUsername());
//		} else {
//			ProjectLeader pl = (ProjectLeader) user;
//			System.out.println("Jestem liderem projektu! Moje projekty:" + pl.getUsername());
//			for (Project project1 : pl.getProjects()) {
//				System.out.println(project1.getProjectName());
//			}
//		}
		
		// Przyk�ad u�ycia getObjectsList - pobiera z bazy list� obiekt�w o podanym typie
//		List<Task> tasks = (List<Task>) Database.getObjectsList(Types.ITTask);
//		for (Task task1 : tasks) {
//			System.out.println(task1.getName());
//			if (task1.canTakeTask()) {
//				System.out.println("Dost�pny");
//			} else {
//				System.out.println(task1.getUser().getUsername() + " zaj�� mnie");
//			}
//		}
		
	}
}
