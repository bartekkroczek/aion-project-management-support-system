package pojo;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ProgrammerGUI", catalog = "AIONdb")
@PrimaryKeyJoinColumn(name = "id")
public class ProgrammerGUI extends GUI implements Serializable {
	
	public ProgrammerGUI() {
		super();
	}

	public ProgrammerGUI(Long id, Set<User> users) {
		super(id, users);
	}

	@Override
	public void drawGUI() {
		
	}
	
}
