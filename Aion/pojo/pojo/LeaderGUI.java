package pojo;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "LeaderGUI", catalog = "AIONdb")
@PrimaryKeyJoinColumn(name = "id")
public class LeaderGUI extends GUI implements Serializable {
	
	public LeaderGUI() {
		super();
	}

	public LeaderGUI(Long id, Set<User> users) {
		super(id, users);
	}

	@Override
	public void drawGUI() {
		
	}
	
}