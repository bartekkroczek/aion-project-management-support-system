package pojo;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "Milestone", catalog = "AIONdb")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Milestone implements java.io.Serializable {

	private Long id;
	private Project project;
	private String name;
	private String description;
	private double percentComplete;
	private Set<Task> tasks = new HashSet<Task>(0);

	public Milestone() {
	}

	public Milestone(Project project, String name, double percentComplete) {
		this.project = project;
		this.name = name;
		this.percentComplete = percentComplete;
	}

	public Milestone(Project project, String name, String description,
			double percentComplete, Set<Task> tasks) {
		this.project = project;
		this.name = name;
		this.description = description;
		this.percentComplete = percentComplete;
		this.tasks = tasks;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Project_id")
	public Project getProject() {
		return this.project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	@Column(name = "Name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "Description")
	@Type(type="text")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "PercentComplete", nullable = false, precision = 22, scale = 0)
	public double getPercentComplete() {
		return this.percentComplete;
	}

	public void setPercentComplete(double percentComplete) {
		this.percentComplete = percentComplete;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "milestone")
	public Set<Task> getTasks() {
		return this.tasks;
	}

	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}
	
	public void countStatistics() {
		int finished = howMuchFinishedTasks();
		percentComplete = finished / tasks.size();
	}
	
	public void addTask(Task task) {
		tasks.add(task);
	}
	
	public void deleteTask(Task task) {
		tasks.remove(task);
	}
	
	public boolean done() {
		if (howMuchFinishedTasks() == tasks.size()) {
			return true;
		} else {
			return false;
		}
	}
	
	public int howMuchFinishedTasks() {
		int finished = 0;
		for (Task task : tasks) {
			if (task.getStatus().getCurrentStatus() == Statuses.FINISHED) {
				finished++;
			}
		}
		return finished;
	}

}
