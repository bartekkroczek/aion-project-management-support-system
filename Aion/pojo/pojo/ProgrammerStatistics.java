package pojo;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ProgrammerStatistics", catalog = "AIONdb")
@PrimaryKeyJoinColumn(name = "id")
public class ProgrammerStatistics extends Statistics implements Serializable {
	
	public ProgrammerStatistics() {
		super();
	}

	public ProgrammerStatistics(Long id, Set<User> users) {
		super(id, users);
	}

	@Override
	public void showStats() {
		
	}

}
