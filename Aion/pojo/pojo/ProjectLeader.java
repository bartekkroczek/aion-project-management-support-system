package pojo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "ProjectLeader", catalog = "AIONdb")
@PrimaryKeyJoinColumn(name = "id")
public class ProjectLeader extends User implements java.io.Serializable {

	private Set<Project> projects = new HashSet<Project>(0);

	public ProjectLeader() {
	}

	public ProjectLeader(String username, String password, String name,
			String surname, Short age, String emailAddress, String phoneNumber,
			int howManyDoneTasks, int howManyAbandonedTasks,
			Date lastLogged, Task task, Set<Project> projects) {
		super(username, password, name, surname, age, emailAddress,
				phoneNumber, howManyDoneTasks, howManyAbandonedTasks,
				lastLogged, task);
		this.projects = projects;
	}

	public ProjectLeader(String username, String password, String emailAddress,
			int howManyDoneTasks, int howManyAbandonedTasks, Set<Project> projects) {
		super(username, password, emailAddress, howManyDoneTasks,
				howManyAbandonedTasks);
		this.projects = projects;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "projectLeader")
	@Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
	public Set<Project> getProjects() {
		return this.projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}
	
	public Project getProjectByName(String projectName) {
		for (Project project : projects) {
			if (project.getProjectName().equals(projectName)) {
				return project;
			}
		}
		return null;
	}

}
