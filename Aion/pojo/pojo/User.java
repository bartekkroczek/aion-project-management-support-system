package pojo;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "User", catalog = "AIONdb", uniqueConstraints = {
		@UniqueConstraint(columnNames = "EmailAddress"),
		@UniqueConstraint(columnNames = "Username") })
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class User implements java.io.Serializable {

	private Long id;
	private String username;
	private String password;
	private String name;
	private String surname;
	private Short age;
	private String emailAddress;
	private String phoneNumber;
	private int howManyDoneTasks;
	private int howManyAbandonedTasks;
	private Date lastLogged;
	private Task task;
	private GUI gui;
	private Statistics stats;

	public User() {
	}

	public User(String username, String password, String emailAddress,
			int howManyDoneTasks, int howManyAbandonedTasks) {
		this.username = username;
		this.password = password;
		this.emailAddress = emailAddress;
		this.howManyDoneTasks = howManyDoneTasks;
		this.howManyAbandonedTasks = howManyAbandonedTasks;
	}

	public User(String username, String password, String name, String surname,
			Short age, String emailAddress, String phoneNumber,
			int howManyDoneTasks, int howManyAbandonedTasks, Date lastLogged,
			Task task) {
		this.username = username;
		this.password = password;
		this.name = name;
		this.surname = surname;
		this.age = age;
		this.emailAddress = emailAddress;
		this.phoneNumber = phoneNumber;
		this.howManyDoneTasks = howManyDoneTasks;
		this.howManyAbandonedTasks = howManyAbandonedTasks;
		this.lastLogged = lastLogged;
		this.task = task;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "Username", unique = true, length = 45)
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "Password", length = 56)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "Name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "Surname", length = 45)
	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@Column(name = "Age")
	public Short getAge() {
		return this.age;
	}

	public void setAge(Short age) {
		this.age = age;
	}

	@Column(name = "EmailAddress", unique = true, length = 45)
	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Column(name = "PhoneNumber", length = 45)
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Column(name = "HowManyDoneTasks", nullable = false)
	public int getHowManyDoneTasks() {
		return this.howManyDoneTasks;
	}

	public void setHowManyDoneTasks(int howManyDoneTasks) {
		this.howManyDoneTasks = howManyDoneTasks;
	}

	@Column(name = "HowManyAbandonedTasks", nullable = false)
	public int getHowManyAbandonedTasks() {
		return this.howManyAbandonedTasks;
	}

	public void setHowManyAbandonedTasks(int howManyAbandonedTasks) {
		this.howManyAbandonedTasks = howManyAbandonedTasks;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "LastLogged", length = 10)
	public Date getLastLogged() {
		return this.lastLogged;
	}

	public void setLastLogged(Date lastLogged) {
		this.lastLogged = lastLogged;
	}

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "user", cascade = CascadeType.ALL)
	public Task getTask() {
		return this.task;
	}

	public void setTask(Task task) {
		this.task = task;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "GUI_id")
	public GUI getGui() {
		return gui;
	}

	public void setGui(GUI gui) {
		this.gui = gui;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Statistics_id")
	public Statistics getStats() {
		return stats;
	}

	public void setStats(Statistics stats) {
		this.stats = stats;
	}

	public void drawGUI() {
		gui.drawGUI();
	}

}
