package pojo;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "Task", catalog = "AIONdb")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Task implements java.io.Serializable {

	private Long id;
	private User user;
	private Project project;
	private Milestone milestone;
	private String name;
	private String description;
	private Date startDate;
	private Date finishDate;
	private Date takeDate;
	private Date executionDate;
	private Status status;
	private Tags tag;

	public Task() {
	}

	public Task(User user, Project project, Milestone milestone, String name,
			Status status) {
		this.user = user;
		this.project = project;
		this.milestone = milestone;
		this.name = name;
		this.status = status;
	}

	public Task(User user, Project project, Milestone milestone, String name,
			String description, Date startDate, Date finishDate, Date takeDate,
			Date executionDate, Status status) {
		this.user = user;
		this.project = project;
		this.milestone = milestone;
		this.name = name;
		this.description = description;
		this.startDate = startDate;
		this.finishDate = finishDate;
		this.takeDate = takeDate;
		this.executionDate = executionDate;
		this.status = status;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "User_id")
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Project_id")
	public Project getProject() {
		return this.project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Milestone_id")
	public Milestone getMilestone() {
		return this.milestone;
	}

	public void setMilestone(Milestone milestone) {
		this.milestone = milestone;
	}

	@Column(name = "Name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "Description")
	@Type(type="text")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "StartDate", length = 10)
	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FinishDate", length = 10)
	public Date getFinishDate() {
		return this.finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "TakeDate", length = 10)
	public Date getTakeDate() {
		return this.takeDate;
	}

	public void setTakeDate(Date takeDate) {
		this.takeDate = takeDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "ExecutionDate", length = 10)
	public Date getExecutionDate() {
		return this.executionDate;
	}

	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Status_id")
	@Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "Tag")
	public Tags getTag() {
		return this.tag;
	}
	
	public void setTag(Tags tag) {
		this.tag = tag;
	}
	
	public boolean canTakeTask() {
		return this.status.canTakeTask();
	}
	
	public void makeFinished() {
		status.makeFinished();
	}
	
	public void makeTaken() {
		status.makeTaken();
	}
	
	public void makeAvailable() {
		status.makeAvailable();
	}
	
}
