package pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ITTask", catalog = "AIONdb")
@PrimaryKeyJoinColumn(name = "id")
public class ITTask extends Task implements java.io.Serializable {

	public ITTask() {
		super();
	}

	public ITTask(User user, Project project, Milestone milestone, String name,
			String description, Date startDate, Date finishDate, Date takeDate,
			Date executionDate, Status state) {
		super(user, project, milestone, name, description, startDate,
				finishDate, takeDate, executionDate, state);
	}

	public ITTask(User user, Project project, Milestone milestone, String name,
			Status state) {
		super(user, project, milestone, name, state);
	}

}
