package pojo;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "LeaderStatistics", catalog = "AIONdb")
@PrimaryKeyJoinColumn(name = "id")
public class LeaderStatistics extends Statistics implements Serializable {
	
	private int howManyProjects;
	private int howManyDoneProjects;
	private int howManyDeletedProjects;
	
	public LeaderStatistics() {
		super();
	}

	public LeaderStatistics(Long id, Set<User> users) {
		super(id, users);
	}

	public LeaderStatistics(int howManyProjects, int howManyDoneProjects,
			int howManyDeletedProjects) {
		super();
		this.howManyProjects = howManyProjects;
		this.howManyDoneProjects = howManyDoneProjects;
		this.howManyDeletedProjects = howManyDeletedProjects;
	}
	
	public LeaderStatistics(Long id, Set<User> users,
			int howManyProjects, int howManyDoneProjects,
			int howManyDeletedProjects) {
		super(id, users);
		this.howManyProjects = howManyProjects;
		this.howManyDoneProjects = howManyDoneProjects;
		this.howManyDeletedProjects = howManyDeletedProjects;
	}

	@Column(name = "HowManyProjects", nullable = false)
	public int getHowManyProjects() {
		return this.howManyProjects;
	}

	public void setHowManyProjects(int howManyProjects) {
		this.howManyProjects = howManyProjects;
	}

	@Column(name = "HowManyDoneProjects", nullable = false)
	public int getHowManyDoneProjects() {
		return this.howManyDoneProjects;
	}

	public void setHowManyDoneProjects(int howManyDoneProjects) {
		this.howManyDoneProjects = howManyDoneProjects;
	}

	@Column(name = "HowManyDeletedProjects", nullable = false)
	public int getHowManyDeletedProjects() {
		return this.howManyDeletedProjects;
	}

	public void setHowManyDeletedProjects(int howManyDeletedProjects) {
		this.howManyDeletedProjects = howManyDeletedProjects;
	}
	
	@Override
	public void showStats() {

	}

}