package pojo;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "Status", catalog = "AIONdb")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Status implements Serializable {
	
	private Long id;
	private Set<Task> tasks = new HashSet<Task>(0);
	private Statuses currentStatus;
	
	public Status() {
		currentStatus = Statuses.AVAILABLE;
	}
	
	public Status(Long id, Set<Task> tasks, Statuses currentStatus) {
		this.id = id;
		this.tasks = tasks;
		this.currentStatus = currentStatus;
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "status")
	public Set<Task> getTasks() {
		return this.tasks;
	}

	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "Status")
	public Statuses getCurrentStatus() {
		return currentStatus;
	}
	
	public void setCurrentStatus(Statuses currentStatus) {
		this.currentStatus = currentStatus;
	}

	public abstract boolean canTakeTask();

	public abstract void makeTaken();
	
	public abstract void makeFinished();

	public abstract void makeAvailable();
	
	public abstract void makeNotAvailable();

}
