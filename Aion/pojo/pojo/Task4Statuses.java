package pojo;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "Task4Statuses", catalog = "AIONdb")
@PrimaryKeyJoinColumn(name = "id")
public class Task4Statuses extends Status implements Serializable {
	
	public Task4Statuses() {
		super();
	}

	public Task4Statuses(Long id, Set<Task> tasks, Statuses currentStatus) {
		super(id, tasks, currentStatus);
	}

	@Override
	public boolean canTakeTask() {
		if (getCurrentStatus() == Statuses.AVAILABLE) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void makeTaken() {
		setCurrentStatus(Statuses.TAKEN);
	}

	@Override
	public void makeFinished() {
		setCurrentStatus(Statuses.FINISHED);
	}

	@Override
	public void makeAvailable() {
		setCurrentStatus(Statuses.AVAILABLE);
	}
	
	@Override
	public void makeNotAvailable() {
		setCurrentStatus(Statuses.NOT_AVAILABLE);
	}

}
