package pojo;

import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "Programmer", catalog = "AIONdb")
@PrimaryKeyJoinColumn(name = "id")
public class Programmer extends User implements java.io.Serializable {

	public Programmer() {
		super();
	}

	public Programmer(String username, String password, String emailAddress,
			int howManyDoneTasks, int howManyAbandonedTasks) {
		super(username, password, emailAddress, howManyDoneTasks,
				howManyAbandonedTasks);
	}

	public Programmer(String username, String password, String name,
			String surname, Short age, String emailAddress, String phoneNumber,
			int howManyDoneTasks, int howManyAbandonedTasks,
			Date lastLogged, Task task) {
		super(username, password, name, surname, age, emailAddress,
				phoneNumber, howManyDoneTasks, howManyAbandonedTasks,
				lastLogged, task);
	}

}
