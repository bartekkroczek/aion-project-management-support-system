package pojo;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ITMilestone", catalog = "AIONdb")
@PrimaryKeyJoinColumn(name = "id")
public class ITMilestone extends Milestone implements java.io.Serializable {

	public ITMilestone() {
		super();
	}

	public ITMilestone(Project project, String name, double percentComplete) {
		super(project, name, percentComplete);
	}

	public ITMilestone(Project project, String name, String description,
			double percentComplete, Set<Task> tasks) {
		super(project, name, description, percentComplete, tasks);
	}

}
