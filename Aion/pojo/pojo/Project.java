package pojo;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "Project", catalog = "AIONdb", uniqueConstraints = @UniqueConstraint(columnNames = "ProjectName"))
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Project implements java.io.Serializable {

	private Long id;
	private ProjectLeader projectLeader;
	private String projectName;
	private String description;
	private double percentComplete;
	private int howManyTasks;
	private Date startDate;
	private Date finishDate;
	private Set<Task> tasks = new HashSet<Task>(0);
	private Set<Milestone> milestones = new HashSet<Milestone>(0);

	public Project() {
	}

	public Project(ProjectLeader projectLeader, String projectName,
			double percentComplete, int howManyTasks) {
		this.projectLeader = projectLeader;
		this.projectName = projectName;
		this.percentComplete = percentComplete;
		this.howManyTasks = howManyTasks;
	}

	public Project(ProjectLeader projectLeader, String projectName,
			String description, double percentComplete, int howManyTasks,
			Date startDate, Date finishDate, Set<Task> tasks,
			Set<Milestone> milestones) {
		this.projectLeader = projectLeader;
		this.projectName = projectName;
		this.description = description;
		this.percentComplete = percentComplete;
		this.howManyTasks = howManyTasks;
		this.startDate = startDate;
		this.finishDate = finishDate;
		this.tasks = tasks;
		this.milestones = milestones;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ProjectLeader_User_id")
	public ProjectLeader getProjectLeader() {
		return this.projectLeader;
	}

	public void setProjectLeader(ProjectLeader projectLeader) {
		this.projectLeader = projectLeader;
	}

	@Column(name = "ProjectName", unique = true, length = 45)
	public String getProjectName() {
		return this.projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	@Column(name = "Description")
	@Type(type="text")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "PercentComplete", nullable = false, precision = 22, scale = 0)
	public double getPercentComplete() {
		return this.percentComplete;
	}

	public void setPercentComplete(double percentComplete) {
		this.percentComplete = percentComplete;
	}

	@Column(name = "HowManyTasks", nullable = false)
	public int getHowManyTasks() {
		return this.howManyTasks;
	}

	public void setHowManyTasks(int howManyTasks) {
		this.howManyTasks = howManyTasks;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "StartDate", length = 10)
	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FinishDate", length = 10)
	public Date getFinishDate() {
		return this.finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "project")
	@Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
	public Set<Task> getTasks() {
		return this.tasks;
	}

	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "project")
	@Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
	public Set<Milestone> getMilestones() {
		return this.milestones;
	}

	public void setMilestones(Set<Milestone> milestones) {
		this.milestones = milestones;
	}
	
	public Milestone getMilestoneByName(String milestoneName) {
		for(Milestone milestone : milestones) {
			if(milestone.getName().equals(milestoneName)) {
				return milestone;
			}
		}
		return null;
	}	
}
