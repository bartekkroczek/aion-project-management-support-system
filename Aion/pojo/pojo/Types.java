package pojo;

public enum Types {
    ITMilestone, ITProject, ITTask, LeaderGUI, LeaderStatistics,
	Programmer, ProgrammerGUI, ProgrammerStatistics, ProjectLeader, 
	Task3Statuses
}
