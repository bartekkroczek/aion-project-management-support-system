package pojo;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.hibernate.tool.hbm2ddl.SchemaExport;

public class Database {

	/**
	 * Export your schema to configuration file and database
	 */
	public static void exportSchema() {
		AnnotationConfiguration configuration = new AnnotationConfiguration();

		configuration.addAnnotatedClass(User.class)
				.addAnnotatedClass(Programmer.class)
				.addAnnotatedClass(ProjectLeader.class)
				.addAnnotatedClass(Project.class)
				.addAnnotatedClass(ITProject.class)
				.addAnnotatedClass(Milestone.class)
				.addAnnotatedClass(ITMilestone.class)
				.addAnnotatedClass(Task.class).addAnnotatedClass(ITTask.class)
				.addAnnotatedClass(GUI.class)
				.addAnnotatedClass(LeaderGUI.class)
				.addAnnotatedClass(LeaderStatistics.class)
				.addAnnotatedClass(ProgrammerGUI.class)
				.addAnnotatedClass(ProgrammerStatistics.class)
				.addAnnotatedClass(Statistics.class)
				.addAnnotatedClass(Status.class)
				.addAnnotatedClass(Task4Statuses.class);

		configuration.configure("hibernate.cfg.xml");

		SchemaExport schema = new SchemaExport(configuration);

		schema.create(true, true);
	}

	public static List<?> getObjectsList(Types type) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<?> list = null;
		try {
			transaction = session.beginTransaction();
			String argument = type.toString();
			list = session.createQuery("from " + argument).list();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
		return list;
	}

	public static User getUser(String username, String password)
			throws Exception {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		User user = null;
		try {
			transaction = session.beginTransaction();
			String hql = "from User U where U.username = :user_username "
					+ "and U.password = :user_password";
			Query query = session.createQuery(hql);
			query.setString("user_username", username);
			query.setString("user_password", password);
			user = (User) query.uniqueResult();
			if (user == null) {
				throw new Exception("No user");
			}
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
		return user;
	}

	public static void saveObject(Object o) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(o);
			transaction.commit();
			session.flush();
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public static void updateObject(Object o) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.update(o);
			transaction.commit();
			session.flush();
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public static void saveOrUpdate(Object o) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		long id;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(o);
			transaction.commit();
			session.flush();
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public static void removeObject(Object o) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.delete(o);
			transaction.commit();
			session.flush();
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public static Object getObjectByID(Types type, Integer id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Object o = null;
		try {
			transaction = session.beginTransaction();
			String argument = type.toString();
			Query query = session.createQuery("from " + argument + " as p "
					+ "where p.id" + " = " + id);
			query.setCacheable(true);
			o = query.uniqueResult();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
		return o;
	}

	public static Long getNextAvailableID(Types type) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Long key = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("select max(id) from "
					+ type.toString());
			key = (Long) query.uniqueResult();
			key++;
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
		return key;
	}

	public static void resetPassword(String username, String email,
			String password) throws Exception {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		User user = null;
		try {
			transaction = session.beginTransaction();
			String hql = "update User U set U.password = :user_password" +
					" where U.username = :user_username "
					+ "and U.emailAddress = :user_emailAddress";
			Query query = session.createQuery(hql);
			query.setString("user_password", password);
			query.setString("user_username", username);
			query.setString("user_emailAddress", email);
			int rowCount = query.executeUpdate();
			if (rowCount == 0) {
				throw new Exception("No user");
			}
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}
	
	public static void changePassword(String username, String currentPassword,
			String newPassword) throws Exception {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		User user = null;
		try {
			transaction = session.beginTransaction();
			String hql = "update User U set U.password = :user_newPassword" +
					" where U.username = :user_username "
					+ "and U.password = :user_currentPassword";
			Query query = session.createQuery(hql);
			query.setString("user_currentPassword", currentPassword);
			query.setString("user_username", username);
			query.setString("user_newPassword", newPassword);
			int rowCount = query.executeUpdate();
			if (rowCount == 0) {
				throw new Exception("No user");
			}
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

}
