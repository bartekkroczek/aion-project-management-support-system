package pojo;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ITProject", catalog = "AIONdb")
@PrimaryKeyJoinColumn(name = "id")
public class ITProject extends Project implements java.io.Serializable {

	public ITProject() {
		super();
	}

	public ITProject(ProjectLeader projectLeader, String projectName,
			double percentComplete, int howManyTasks) {
		super(projectLeader, projectName, percentComplete, howManyTasks);
	}

	public ITProject(ProjectLeader projectLeader, String projectName,
			String description, double percentComplete, int howManyTasks,
			Date startDate, Date finishDate, Set<Task> tasks,
			Set<Milestone> milestones) {
		super(projectLeader, projectName, description, percentComplete,
				howManyTasks, startDate, finishDate, tasks, milestones);
	}

}
