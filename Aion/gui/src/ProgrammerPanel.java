import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.MouseInputAdapter;

import pojo.Database;
import pojo.ITTask;
import pojo.Programmer;
import pojo.Task;
import pojo.Types;

public class ProgrammerPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3475699014028789740L;
	private JLabel currentTaskLabel;
	private JButton takeButton;
	private JButton submitButton;
	private JButton giveupButton;
	private JScrollPane taskPane;
	private JScrollPane sortOptionPane;
	private JList taskList;
	private JList sortOptionList;
	private Programmer programmer = null;
	private List<ITTask> tasks;

	/**
	 * Create the panel.
	 */
	public ProgrammerPanel() {

		String[] someTestOptions = { "All", "Avaiable", "Not avaiable" };

		currentTaskLabel = new JLabel(
				"<html><b><font color=RED>No Task is undertaken</font></b></html>");
		takeButton = new JButton("Take");
		submitButton = new JButton("Submit");
		giveupButton = new JButton("Give up");
		taskList = new JList<String>();
		sortOptionList = new JList<String>(someTestOptions);
		taskPane = new JScrollPane(taskList);
		sortOptionPane = new JScrollPane(sortOptionList);

		sortOptionList.setSelectedIndex(0);

		currentTaskLabel.addMouseListener(new MouseInputAdapter() {
			public void mouseEntered(MouseEvent e) {
				if(programmer.getTask() != null) {
					currentTaskLabel.setText("<html><b>Current Task: "
							+ "<font color=BLUE>" + programmer.getTask().getName()
							+ "</font></b></html>");
				}	
			}
			
			public void mouseExited(MouseEvent e) {
				if(programmer.getTask() != null) {
					currentTaskLabel.setText("<html><b>Current Task: "
							+ "<font color=GREEN>" + programmer.getTask().getName()
							+ "</font></b></html>");
				}
			}
			
			public void mouseClicked(MouseEvent e) {
				if(programmer.getTask() != null)
					displayTaskDialog(programmer.getTask());
			}
		});
		taskList.addMouseListener(new MouseInputAdapter() {
			public void mousePressed(MouseEvent arg0) {
				if (arg0.getClickCount() >= 2) {
					ITTask task = getSelectedTask();
					if(task == null)
						return;
					displayTaskDialog(task);					
				}
			}
		});

		sortOptionList.addMouseListener(new MouseInputAdapter() {
			public void mousePressed(MouseEvent arg0) {
				JList list = (JList) arg0.getSource();
				int index = list.locationToIndex(arg0.getPoint());
				if (index >= 0) {
					updateWithFilter(index);
				}
			}
		});

		submitButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ITTask programmerTask = (ITTask) programmer.getTask();
				if (programmerTask == null) {
					System.out.println("You have no task to submit!");
					return;
				}

				programmerTask.getStatus().makeFinished();
				// tego sie troche obawiam ;D
				programmer.setTask(null);
				Database.saveOrUpdate(programmer);
				Database.saveOrUpdate(programmerTask);
				updateProgrammerTask();
			}
		});

		takeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ITTask programmerTask = (ITTask) programmer.getTask();
				if (programmerTask != null) {
					System.out.println("You have to finish your task first!");
					return;
				}

				ITTask task = getSelectedTask();
				if (task == null)
					return;

				if (task.getStatus().canTakeTask()) {
					task.getStatus().makeTaken();
					programmer.setTask(task);
					Database.saveOrUpdate(programmer);
					updateProgrammerTask();
					return;
				} else {
					System.out.println("This task is not avaiable");
					return;
				}
			}
		});

		giveupButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ITTask programmerTask = (ITTask) programmer.getTask();
				if (programmerTask == null) {
					System.out.println("Nothing to give up");
					return;
				}

				programmerTask.makeAvailable();
				programmer.setTask(null);
				Database.saveOrUpdate(programmer);
				Database.saveOrUpdate(programmerTask);
				updateProgrammerTask();
			}
		});

		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();

		gc.weightx = 0.5;
		gc.weighty = 0.5;

		gc.gridx = 1;
		gc.gridy = 0;
		gc.gridwidth = 2;
		gc.fill = GridBagConstraints.BOTH;
		add(currentTaskLabel, gc);

		gc.gridx = 0;
		gc.gridy = 1;
		gc.gridwidth = 1;
		add(new JLabel("Tasks"), gc);

		gc.gridx = 1;
		gc.gridy = 1;
		gc.gridwidth = 1;
		add(new JLabel("Sort by"), gc);

		gc.weightx = 10;
		gc.gridx = 0;
		gc.gridy = 2;
		gc.gridwidth = 1;
		gc.gridheight = 4;
		gc.fill = GridBagConstraints.BOTH;
		add(taskPane, gc);

		gc.gridx = 1;
		gc.gridy = 2;
		gc.gridwidth = 1;
		gc.gridheight = 2;
		gc.fill = GridBagConstraints.BOTH;
		add(sortOptionPane, gc);

		gc.weightx = 0.5;
		gc.gridx = 2;
		gc.gridy = 2;
		gc.gridwidth = 1;
		gc.gridheight = 1;
		gc.anchor = GridBagConstraints.WEST;
		gc.fill = GridBagConstraints.VERTICAL;
		add(submitButton, gc);

		gc.gridx = 2;
		gc.gridy = 3;
		gc.gridwidth = 1;
		gc.gridheight = 1;
		add(giveupButton, gc);

		gc.gridx = 2;
		gc.gridy = 4;
		gc.gridwidth = 1;
		gc.gridheight = 1;
		add(takeButton, gc);
	}

	public Programmer getProgrammer() {
		return programmer;
	}

	public void setProgrammer(Programmer programmer) {
		this.programmer = programmer;
	}

	private void displayTaskDialog(Task task) {
		final JComponent[] inputs = new JComponent[] {
				new JLabel("<html>Title: <font color=GREEN>" + task.getName() + "</font></html>"),
				new JLabel("<html>Description: <b>" + task.getDescription() + "</b></font></html>")};
		JOptionPane.showMessageDialog(null, inputs, "Task details",
				JOptionPane.PLAIN_MESSAGE);
	}
	
	private void updateWithFilter(int filterIndex) {
		if (tasks == null)
			return;

		ArrayList<String> stringArrayList = new ArrayList<>();
		taskList.removeAll();

		for (ITTask task : tasks) {
			switch (filterIndex) {
			case 0:
				// all tasks
				break;
			case 1:
				// avaiable tasks
				if (task.canTakeTask() == false)
					continue;
				break;
			case 2:
				// taken or not avaiable
				if (task.canTakeTask())
					continue;
				break;
			}
			stringArrayList.add(task.getName());
		}
		taskList.setListData(stringArrayList.toArray());
	}

	private void updateProgrammerTask() {
		ITTask programmerTask = (ITTask) programmer.getTask();

		if (programmerTask != null) {
			currentTaskLabel.setText("<html><b>Current Task: "
					+ "<font color=GREEN>" + programmerTask.getName()
					+ "</font></b></html>");
		} else {
			currentTaskLabel
					.setText("<html><b><font color=RED>No Task is undertaken</font></b></html>");
		}
	}

	public void update() {
		if (programmer != null) {
			updateProgrammerTask();
			tasks = (List<ITTask>) Database.getObjectsList(Types.ITTask);
			updateWithFilter(0);
		}
	}

	private ITTask getSelectedTask() {
		String taskName = (String) taskList.getSelectedValue();

		if (taskName == null) {
			return null;
		}

		for (ITTask task : tasks) {
			if (task.getName().equals(taskName)) {
				return task;
			}
		}
		return null;
	}
}
