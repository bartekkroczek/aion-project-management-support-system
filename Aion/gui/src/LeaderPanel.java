import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import pojo.Project;
import pojo.User;

import edu.uci.ics.jung.visualization.control.ModalGraphMouse;

import graph.GraphClass;
import graph.GraphClass.TaskGraphPanel;
import graph.MilestoneContainer;
import graph.TaskContainer;

public class LeaderPanel extends JPanel {

        private static final long serialVersionUID = -4843251354686468412L;
        private JButton editButton;
        private JButton pickButton;
        private JButton viewButton;
        private JButton saveButton;
        private JPanel optionPanel;
        private JPanel graphPanel;
        private Project project;
        private User user;
        private GraphClass graphClass;

        /**
         * create LeaderPanel
         */

        public LeaderPanel() {
        		project = null;
        		user = null;
                optionPanel = new JPanel();
                graphPanel = new JPanel();
                graphClass = new GraphClass(null, null);

                setLayout(new BorderLayout());

                optionPanel.setBorder(new TitledBorder(null, "Options",
                                TitledBorder.LEADING, TitledBorder.TOP, null, null));

                Box verticalBox = new Box(BoxLayout.Y_AXIS);

                editButton = new JButton("Editing");
                editButton.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                                graphClass.graphPanel.gm.setMode(ModalGraphMouse.Mode.EDITING);
                        }
                });
                
                pickButton = new JButton("Picking");
                pickButton.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                                graphClass.graphPanel.gm.setMode(ModalGraphMouse.Mode.PICKING);
                        }
                });
                
                viewButton = new JButton("View");
                viewButton.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                                graphClass.graphPanel.gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
                        }
                });

                saveButton = new JButton("save graph");
                saveButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try {
                        	String filename = project.getId().toString();
                            graphClass.graphPanel.saveGraphToFile(filename);
                        } catch (IOException exception) {
                            System.out.println(exception.getStackTrace());
                        }
                    }
                });
                
                verticalBox.add(viewButton);
                verticalBox.add(pickButton);
                verticalBox.add(editButton);
                verticalBox.add(saveButton);
                
                optionPanel.add(verticalBox);
                add(optionPanel, BorderLayout.WEST);

                graphPanel.add(graphClass.graphPanel);
                add(graphPanel, BorderLayout.CENTER);
        }
        
        public void setProject(Project project)
        {
        	this.project = project;
        	graphClass.graphPanel.tasks = new TaskContainer(project);
        	graphClass.graphPanel.milestones = new MilestoneContainer(project);
        	graphClass.graphPanel.updateVertexFactory();
        	graphClass.graphPanel.init();
        }
        
        public Project getProject()
        {
        	return this.project;
        }
        
        public void setUser(User project)
        {
        	this.user = project;
        }
        
        public User getUser()
        {
        	return this.user;
        }
        
        public void updateGraph()
        {
        	graphClass.graphPanel.vv.repaint();
        }
        
        public TaskGraphPanel getGraphPanel()
        {
        	return graphClass.graphPanel;
        }

}