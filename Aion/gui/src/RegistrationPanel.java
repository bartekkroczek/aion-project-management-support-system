import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import pojo.Database;
import pojo.Programmer;
import pojo.ProjectLeader;
import server.Mail;

import events.RegistrationEvent;

public class RegistrationPanel extends JPanel {

	/**
        * 
        */
	private static final long serialVersionUID = -687991492884005033L;

	public PanelListenerManager<RegistrationEvent> listenerManager = new PanelListenerManager<>();
	private JButton registerButton;
	private JButton cancelButton;
	private JTextField idTextField;
	private JTextField emailTextField;
	private JPasswordField passwordField;
	private JPasswordField confirmPasswordField;
	private String types[] = { "Programmer", "Leader" };
	private JComboBox accountTypeComboBox;

	private static int TEXT_SIZE = 10;

	/**
	 * Create RegistrationPanel
	 */
	public RegistrationPanel() {
		registerButton = new JButton("Register");
		cancelButton = new JButton("Cancel");
		idTextField = new JTextField(TEXT_SIZE);
		emailTextField = new JTextField(TEXT_SIZE);
		passwordField = new JPasswordField(TEXT_SIZE);
		confirmPasswordField = new JPasswordField(TEXT_SIZE);
		accountTypeComboBox = new JComboBox(types);

		registerButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (idTextField.getText().isEmpty()) {
					System.out.println("Could not create user with empty id");
					return;
				}

				if (passwordField.getText().isEmpty()) {
					System.out
							.println("Could not create user with empty password");
					return;
				}

				if (emailTextField.getText().isEmpty()) {
					System.out
							.println("Could not create user with empty email");
					return;
				}

				if (confirmPasswordField.getText().equals(
						passwordField.getText()) == false) {
					System.out.println("wrong confirm password");
					return;
				}

				if (accountTypeComboBox.getSelectedIndex() == 0) {
					// tworzymy programiste
					Programmer programmer = new Programmer();
					programmer.setUsername(idTextField.getText());
					programmer.setPassword(passwordField.getText());
					programmer.setEmailAddress(emailTextField.getText());
					try {
						Database.saveOrUpdate(programmer);
						new Mail(emailTextField.getText(), idTextField.getText()).send();
					} catch (Exception ex) {
						System.out.println("Registration failed");
						return;
					}
				} else {
					// tworzymy lidera
					ProjectLeader projectLeader = new ProjectLeader();
					projectLeader.setUsername(idTextField.getText());
					projectLeader.setPassword(passwordField.getText());
					projectLeader.setEmailAddress(emailTextField.getText());
					
					try {
						Database.saveOrUpdate(projectLeader);
						new Mail(emailTextField.getText(), idTextField.getText()).send();
					} catch (Exception ex) {
						System.out.println("Registration failed");
						return;
					}
				}
				listenerManager.fireEvent(new RegistrationEvent(this, 0));
			}
		});

		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				listenerManager.fireEvent(new RegistrationEvent(this, 1));
			}
		});

		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();

		gc.weightx = 0.5;
		gc.weighty = 0.5;

		gc.gridx = 0;
		gc.gridy = 0;
		gc.fill = GridBagConstraints.NONE;
		add(new JLabel("ID"), gc);

		gc.gridx = 1;
		gc.gridy = 0;
		gc.fill = GridBagConstraints.HORIZONTAL;
		add(idTextField, gc);

		gc.gridx = 0;
		gc.gridy = 1;
		gc.fill = GridBagConstraints.NONE;
		add(new JLabel("Password"), gc);

		gc.gridx = 1;
		gc.gridy = 1;
		gc.fill = GridBagConstraints.HORIZONTAL;
		add(passwordField, gc);

		gc.gridx = 0;
		gc.gridy = 2;
		gc.fill = GridBagConstraints.NONE;
		add(new JLabel("Confirm Password"), gc);

		gc.gridx = 1;
		gc.gridy = 2;
		gc.fill = GridBagConstraints.HORIZONTAL;
		add(confirmPasswordField, gc);

		gc.gridx = 0;
		gc.gridy = 3;
		gc.fill = GridBagConstraints.NONE;
		add(new JLabel("Email"), gc);

		gc.gridx = 1;
		gc.gridy = 3;
		gc.fill = GridBagConstraints.HORIZONTAL;
		add(emailTextField, gc);

		gc.gridx = 0;
		gc.gridy = 4;
		gc.fill = GridBagConstraints.NONE;
		add(new JLabel("Account Type"), gc);

		gc.gridx = 1;
		gc.gridy = 4;
		gc.fill = GridBagConstraints.HORIZONTAL;
		add(accountTypeComboBox, gc);

		gc.gridx = 0;
		gc.gridy = 5;
		gc.fill = GridBagConstraints.NONE;
		add(registerButton, gc);

		gc.gridx = 1;
		gc.gridy = 5;
		gc.fill = GridBagConstraints.NONE;
		add(cancelButton, gc);
	}
}
