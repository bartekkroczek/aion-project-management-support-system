package graph;

import java.util.Set;

import pojo.Project;
import pojo.Milestone;

public class MilestoneContainer {
	private Set<Milestone> container;
	
	public MilestoneContainer(Project project){
		container = project.getMilestones();
	}
	
	public Milestone getMilestoneById(Long id)
	{
		for(Milestone Milestone : container)
		{
			if(Milestone.getId() == id)
			{
				return Milestone;
			}
		}
		return null;
	}
	
	public Milestone getMilestoneByName(String name)
	{
		for(Milestone Milestone : container)
		{
			if(Milestone.getName().equals(name))
			{
				return Milestone;
			}
		}
		return null;
	}
	
	
	public Set<Milestone> getSetOfMilestones()
	{
		return container;
	}
	
	public int getSize()
	{
		return container.size();
	}
}
