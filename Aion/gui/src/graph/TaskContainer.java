package graph;
import java.util.Set;

import pojo.Database;
import pojo.Project;
import pojo.Task;


public class TaskContainer {
	private Set<Task> container;
	
	public TaskContainer(Project project){
		container = project.getTasks();
	}
	
	public void deleteTask(Long id)
	{
		for(Task task : container)
		{
			if(task.getId() == id)
			{
				container.remove(task);
				break;
			}
		}
	}
	
	public void deleteTask(Task task)
	{
		task.getMilestone().getTasks().remove(task);
		Database.removeObject(task);
		container.remove(task);
	}
	
	public Task getTaskById(Long id)
	{
		for(Task task : container)
		{
			if(task.getId() == id)
			{
				return task;
			}
		}
		return null;
	}
	
	public void addTask(Task task)
	{
		container.add(task);
	}
	
	public Set<Task> getSetOfTasks()
	{
		return container;
	}
	
}
