package graph;

import org.apache.commons.collections15.Factory;

public class EdgeFactory implements Factory<Number>{
	int i = 0;
	
	public Number create()
	{
		return i++;
	}
	
	public void reset()
	{
		i = 0;
	}
	
	public int getLast()
	{
		return i - 1;
	}
}
