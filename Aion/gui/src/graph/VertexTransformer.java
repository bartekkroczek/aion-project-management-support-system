package graph;

import java.awt.Color;
import java.awt.Paint;
import java.util.HashMap;
import java.util.Map;

import pojo.Milestone;

import edu.uci.ics.jung.visualization.decorators.PickableVertexPaintTransformer;
import edu.uci.ics.jung.visualization.picking.PickedInfo;

public class VertexTransformer extends PickableVertexPaintTransformer<VertexClass>{

	private Map<Milestone, Color> map;
	
	public VertexTransformer(PickedInfo<VertexClass> pi, Paint fill_paint,
			Paint picked_paint) {
		super(pi, fill_paint, picked_paint);
		map = new HashMap<Milestone, Color>();
	}
	
	public Paint transform(VertexClass vertex){
		if(map.isEmpty()){
			if(pi.isPicked(vertex))
				return this.picked_paint;
			else
				return this.fill_paint;
		}
		else{
			Color color = map.get(vertex.getTask().getMilestone());
			if(pi.isPicked(vertex)){
				return color.brighter();	
			}
			else{
				return color;
			}
		}
	}

	public Map<Milestone, Color> getMap() {
		return map;
	}

	public void setMap(Map<Milestone, Color> map) {
		this.map = map;
	}
	
}
