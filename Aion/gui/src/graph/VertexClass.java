package graph;

import pojo.Task;

/**
 * Created with IntelliJ IDEA.
 * User: kjurek
 * Date: 5/27/13
 * Time: 8:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class VertexClass {
    private Double x = 0.0;
    private Double y = 0.0;
    private String label = "";
    private Task task = null;
    private boolean visited;
    
    public VertexClass(Double x, Double y, String label)
    {
        this.x = x;
        this.y = y;
        this.label = label;
        setVisited(false);
    }


    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }

    public String getLabel() {
        return label;
    }

    public Task getTask(){
    	return task;
    }
    
    public void setX(Double x) {
        this.x = x;
    }

    public void setY(Double y) {
        this.y = y;
    }
    
    public void setLabel(String label) {
        this.label = label;
    }
    
    public void setTask(Task task){
    	this.task = task;
    }


	public boolean isVisited() {
		return visited;
	}


	public void setVisited(boolean visited) {
		this.visited = visited;
	}
    
    
}
