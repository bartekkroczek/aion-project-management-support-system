package graph;

import java.util.Collection;

import pojo.Database;
import pojo.Statuses;

import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.graph.util.Pair;
import agape.tools.Operations;

public class DirectedGraphClass extends DirectedSparseGraph<VertexClass, Number>{
	
	private static final long serialVersionUID = -5355060780856175140L;
	public DirectedGraphClass(){
		super();
	}
	
	public boolean addEdge(Number edge,Pair<? extends VertexClass> endpoints, EdgeType edgeType ){
		
		if(endpoints.getSecond().getTask().getStatus().getCurrentStatus() == Statuses.TAKEN)
		{
			return false;
		}
		
		if( super.addEdge(edge, endpoints, edgeType) ){
			if(endpoints.getSecond().getTask().getStatus().getCurrentStatus() == Statuses.FINISHED)
			{
				this.removeEdge(edge);
				return false;
			}
			if(!Operations.isAcyclic(this))
			{
				this.removeEdge(edge);
				return false;
			}
			
			Collection<VertexClass> collection = this.getPreds_internal(endpoints.getSecond());
			if(collection.isEmpty())
				return true;
			for(VertexClass var : collection){
				if(var.getTask().getStatus().getCurrentStatus() != Statuses.FINISHED){
					endpoints.getSecond().getTask().getStatus().makeNotAvailable();
					Database.saveOrUpdate(endpoints.getSecond().getTask());
					return true;
				}
			}
			
		}
		return false;
	}
	
	public boolean removeEdge(Number edge){
		Pair<VertexClass> endpoints = this.getEndpoints(edge);
		if(endpoints.getSecond().getTask().getStatus().getCurrentStatus() == Statuses.FINISHED || endpoints.getSecond().getTask().getStatus().getCurrentStatus() == Statuses.TAKEN)
		{
			return false;
		}
		if( super.removeEdge(edge) )
		{
			Collection<VertexClass> collection = this.getPreds_internal(endpoints.getSecond());
			if(collection.isEmpty()){
				endpoints.getSecond().getTask().getStatus().makeAvailable();
				Database.saveOrUpdate(endpoints.getSecond().getTask());
				return true;
			}
		}
		
		return false;
	}
	
	
	public boolean addVertex(VertexClass vertex){
		if(vertex != null)
		{
			return super.addVertex(vertex);
		}
		return false;
	}
	
	public boolean removeVertex(VertexClass vertex)
	{
		if( vertex.getTask().getStatus().getCurrentStatus() == Statuses.FINISHED )
		{
			return false;
		}
		return super.removeVertex(vertex);
	}
	
}
