package graph;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import java.io.*;
import java.net.ConnectException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.AbstractAction;

import edu.uci.ics.jung.algorithms.layout.*;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.io.GraphIOException;
import edu.uci.ics.jung.io.GraphMLWriter;
import edu.uci.ics.jung.io.graphml.*;
import edu.uci.ics.jung.visualization.GraphZoomScrollPane;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.AbstractPopupGraphMousePlugin;
import edu.uci.ics.jung.visualization.control.EditingModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.renderers.Renderer;

import org.apache.commons.collections15.Transformer;

import pojo.Database;
import pojo.Milestone;
import pojo.Statuses;
import pojo.Task;
import server.FTPException;
import server.Overwriting;
import customer.FTP;

public class GraphClass {
	
	public TaskGraphPanel graphPanel;

	public GraphClass(TaskContainer tasks, MilestoneContainer milestones){
		graphPanel = new TaskGraphPanel(tasks, milestones);
	}
	
	public class TaskGraphPanel extends JPanel {
        private static final long serialVersionUID = 1L;
		public EditingModalGraphMouse<VertexClass, Number> gm;
		private Graph<VertexClass, Number> graph;
        private StaticLayout<VertexClass, Number> layout;
		public VisualizationViewer<VertexClass, Number> vv;
        private GraphZoomScrollPane scrollPane;
		private VertexFactory vertexFactory;
		private EdgeFactory edgeFactory;
		private VertexTransformer vertexTransformer;
		public TaskContainer tasks;
		public MilestoneContainer milestones;
		private boolean edited = false;
		
		public TaskGraphPanel(TaskContainer tasks, MilestoneContainer milestones) {
			
			graph = getGraph();
			layout = new StaticLayout<VertexClass, Number>(graph);
			vertexFactory = new VertexFactory(tasks, milestones);
			edgeFactory = new EdgeFactory();
		}
		
        private void initVisualizationViewer() {
        	
        	Map<Milestone, Color> map = new HashMap<Milestone, Color>();
			Random rand = new Random();
			for(Milestone var : milestones.getSetOfMilestones()){
				float r = rand.nextFloat();
				float g = rand.nextFloat();
				float b = rand.nextFloat();
				Color randomColor = new Color(r, g, b);
				map.put(var, randomColor.brighter());
			
			}		
            vv = new VisualizationViewer<VertexClass, Number>(layout);
            vertexTransformer = new VertexTransformer(vv.getPickedVertexState(), Color.red, Color.yellow);
            vertexTransformer.setMap(map);
            vv.getRenderContext().setVertexFillPaintTransformer(vertexTransformer);
            vv.getRenderContext().setVertexLabelTransformer(
                    new Transformer<VertexClass, String>() {
                        @Override
                        public String transform(VertexClass vertex) {
                            return vertex.getLabel();
                        }
                    });
            vv.getRenderer().getVertexLabelRenderer()
                    .setPosition(Renderer.VertexLabel.Position.CNTR);
            vv.setBackground(Color.white);
            vv.setPreferredSize(new Dimension(500, 500));
        }

        public void initEditingModalGraphMouse() {
            gm = new EditingModalGraphMouse<VertexClass, Number>(vv.getRenderContext(), vertexFactory, edgeFactory);
            gm.remove(gm.getPopupEditingPlugin());
            gm.add(new PopupGraphMousePlugin());
            gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
            vv.setGraphMouse(gm);
        }
        
        public void init(){
            initVisualizationViewer();
            scrollPane = new GraphZoomScrollPane(vv);
			add(scrollPane);
            initEditingModalGraphMouse();
        }
        
        public void countPrep(){
        	for(VertexClass var : graph.getVertices()){
        		Collection<VertexClass> collection = graph.getPredecessors(var);
        		
        		if(collection.isEmpty()){
        			if(var.getTask().getStatus().getCurrentStatus() == Statuses.NOT_AVAILABLE){
    					var.getTask().getStatus().makeAvailable();
    					Database.saveOrUpdate(var.getTask());
    					break;
    				}
        			continue;
        		}
        		
        		for(VertexClass prep : collection){
        			if(prep.getTask().getStatus().getCurrentStatus() != Statuses.FINISHED){
        				if(var.getTask().getStatus().getCurrentStatus() == Statuses.AVAILABLE){
        					var.getTask().getStatus().makeNotAvailable();
        					Database.saveOrUpdate(var.getTask());
        					break;
        				}
        			}
        		}
        	}
        }
        
		public Graph<VertexClass, Number> getGraph() {
			Graph<VertexClass, Number> g = new DirectedGraphClass();
			
			return g;
		}
		
		private class PopupGraphMousePlugin extends AbstractPopupGraphMousePlugin implements MouseListener {
			public PopupGraphMousePlugin() {
			        this(MouseEvent.BUTTON3_MASK);
			}
			
			public PopupGraphMousePlugin(int modifiers) {
			        super(modifiers);
			}
			
			@SuppressWarnings("unchecked")
			@Override
			protected void handlePopup(MouseEvent e) {
			        Point2D p = e.getPoint();
			        GraphElementAccessor<VertexClass, Number> pickSupport = vv
			                        .getPickSupport();
			        if (pickSupport != null) {
			                final VertexClass v = pickSupport.getVertex(vv.getGraphLayout(),
			                                p.getX(), p.getY());
			                if (v != null) {
			                        JPopupMenu popup = new JPopupMenu();
			                        popup.add(new AbstractAction("remove task") {
			
			                                @Override
			                                public void actionPerformed(ActionEvent arg0) {
			                                	int option = JOptionPane.showConfirmDialog(null, "Are you sure ?", "Delete Task", JOptionPane.YES_NO_OPTION);
		                						if(option == JOptionPane.YES_OPTION){
			                                		tasks.deleteTask(v.getTask());
			                                        graph.removeVertex(v);
			                                        vv.repaint();
		                						}
			                                }
			                        });
			                        popup.add(new AbstractAction("show task informations") {
		                                @Override
		                                public void actionPerformed(ActionEvent arg0) {
		                                	StringBuilder message = new StringBuilder();
		                                	message.append("Name: " + v.getTask().getName() + "\n");
		                                	message.append("Milestone: " + v.getTask().getMilestone().getName() + "\n");
		                                	message.append("Description:\n" + v.getTask().getDescription() + "\n");
		                                	if(v.getTask().getUser() == null){
		                                		message.append("Taken by: none\n");
		                                	}
		                                	else{
		                                		message.append("Taken by: "+ v.getTask().getUser().getName() +"\n");
		                                	}
		                                	message.append("Status: " + v.getTask().getStatus().getCurrentStatus().toString() + "\n");
		                                	JOptionPane.showMessageDialog(null, message.toString(), "Info about Task", JOptionPane.PLAIN_MESSAGE);
		                                }
			                        });
			                        popup.show(vv, e.getX(), e.getY());
			                }
			                final Number edge = pickSupport.getEdge(vv.getGraphLayout(),
	                                p.getX(), p.getY());
			                if (edge != null) {
			                		JPopupMenu popup = new JPopupMenu();
			                		popup.add(new AbstractAction("remove connection") {
	
			                				@Override
			                				public void actionPerformed(ActionEvent arg0) {
			                						int option = JOptionPane.showConfirmDialog(null, "Are you sure ?", "Delete Connection", JOptionPane.YES_NO_OPTION);
			                						if(option == JOptionPane.YES_OPTION){
			                						graph.removeEdge(edge);
			                						vv.repaint();
			                						}
			                				}
			                		});
	                        popup.show(vv, e.getX(), e.getY());
			                }			        
			                
			        }
			}
		}

        public void saveGraphToFile(String filename) throws IOException
        {
            GraphMLWriter<VertexClass, Number> graphMLWriter =
                    new GraphMLWriter<>();
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename)));

            graphMLWriter.setVertexIDs(new Transformer<VertexClass, String>() {
				@Override
				public String transform(VertexClass vertex) {
					Task vertexTask = vertex.getTask();
					if(vertexTask == null)
						System.out.println("TASK IS NULL");
					
					Long taskId = vertexTask.getId();
					
					if(taskId == null) 
						System.out.println("TASK ID IS NULL");
					
					return vertex.getTask().getId().toString();
				}
			});
            
            graphMLWriter.addVertexData("x", null, "0",
                    new Transformer<VertexClass, String>() {
                        @Override
                        public String transform(VertexClass vertex) {
                            return Double.toString(layout.getX(vertex));
                        }
                    });
            graphMLWriter.addVertexData("y", null, "0",
                    new Transformer<VertexClass, String>() {
                        @Override
                        public String transform(VertexClass vertex) {
                            return Double.toString(layout.getY(vertex));
                        }
                    });
            try {
            	graphMLWriter.save(graph, out);
                new FTP("5.39.81.73", 5000).SendFile(filename, Overwriting.allowed);
            } catch (Exception e) {
				System.out.println(e.toString());
			} finally {
				out.close();
			    File file = new File(filename);
                file.delete();
            }
        }

        public void loadGraphFromFile(String filename) throws Exception
        {
        	edited = true;
        	
        	try {
				new FTP("5.39.81.73", 5000).ReceiveFile(filename,
						Overwriting.allowed);
			} catch (Exception e) {
				if(e instanceof ConnectException)
					throw e;
				
				return;
			}
        	
        	BufferedReader fileReader = new BufferedReader(new FileReader(filename));
        	
        	Transformer<GraphMetadata, DirectedGraphClass> graphTransformer =
                    new Transformer<GraphMetadata, DirectedGraphClass>() {
                        @Override
                        public DirectedGraphClass transform(GraphMetadata graphMetadata) {
                            return new DirectedGraphClass();
                        }
                    };
            Transformer<NodeMetadata, VertexClass> vertexTransformer =
                    new Transformer<NodeMetadata, VertexClass>() {
                        @Override
                        public VertexClass transform(NodeMetadata nodeMetadata) {
                            VertexClass v = vertexFactory.create();
                            Long taskId = Long.parseLong(nodeMetadata.getId());
                            
                            Task task = tasks.getTaskById(taskId);
                            v.setTask(task);
                            v.setX(Double.parseDouble(nodeMetadata.getProperty("x")));
                            v.setY(Double.parseDouble(nodeMetadata.getProperty("y")));
                            return v;
                        }
                    };
            Transformer<EdgeMetadata, Number> edgeTransformer =
                    new Transformer<EdgeMetadata, Number>() {
                        @Override
                        public Number transform(EdgeMetadata edgeMetadata) {
                            Number edge = edgeFactory.create();
                            return edge;
                        }
                    };

            Transformer<HyperEdgeMetadata, Number> hyperEdgeTransformer =
                    new Transformer<HyperEdgeMetadata, Number>() {
                        @Override
                        public Number transform(HyperEdgeMetadata hyperEdgeMetadata) {
                            Number edge = edgeFactory.create();
                            return edge;
                        }
                    };
            GraphMLReader2<DirectedGraphClass, VertexClass, Number> graphReader =
                    new GraphMLReader2<DirectedGraphClass, VertexClass, Number>
                            (fileReader, graphTransformer, vertexTransformer,
                             edgeTransformer, hyperEdgeTransformer);
            try {
            	this.vertexFactory.setTrigger(false);
            	this.vertexFactory.reset();
            	edgeFactory.reset();
            	graph = graphReader.readGraph();
            	File file = new File(filename);
            	file.delete();
            } catch(GraphIOException graphex) { 
            	return;
            } finally {
            	fileReader.close();
            	this.vertexFactory.setTrigger(true);
            }
            
            layout = new StaticLayout<VertexClass, Number>(graph, new Transformer<VertexClass, Point2D>() {
                @Override
                public Point2D transform(VertexClass vertex) {
                    Point2D p = new Point2D.Double(vertex.getX(), vertex.getY());
                    return p;
                }
            });
            initVisualizationViewer();
            remove(scrollPane);
            scrollPane = new GraphZoomScrollPane(vv);
            add(scrollPane);
            initEditingModalGraphMouse();
        }
        
    	public void updateVertexFactory() {
    		vertexFactory.setMilestones(milestones);
    		vertexFactory.setTasks(tasks);
    	}
    	
    	public boolean isEdited() {
    		return edited;
    	}
	}
	
	
}
