package graph;

import org.apache.commons.collections15.Factory;

import pojo.Database;
import pojo.ITTask;
import pojo.Milestone;
import pojo.Status;
import pojo.Task;
import pojo.Task4Statuses;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class VertexFactory implements Factory<VertexClass>
{
	private int i = 0;
	private TaskContainer tasks;
	private MilestoneContainer milestones;
	private boolean trigger;
	
	
	public VertexFactory(TaskContainer tasks, MilestoneContainer milestones)
	{
		this.tasks = tasks;
		this.milestones = milestones;
		trigger = true;
	}
	
	public VertexClass create()
	{
		if(trigger)
		{
			String options[] = new String[milestones.getSize()];
			int j = 0;
			
			for(Milestone milestone : milestones.getSetOfMilestones())
			{
				options[j++] = milestone.getName();
			}
			
			JComboBox<String> milestoneComboBox = new JComboBox<String>(options);
			JTextField nameTextField = new JTextField();
			JTextField descriptionTextField = new JTextField();
			
			final JComponent[] inputs = {
					new JLabel("Milestone: "),
					milestoneComboBox,
					new JLabel("Name: "),
					nameTextField,
					new JLabel("Descrition: "),
					descriptionTextField
			};
			
			int option = JOptionPane.showConfirmDialog(null, inputs, "Create Task", JOptionPane.OK_CANCEL_OPTION);
			
			if(option == JOptionPane.OK_OPTION)
			{
				String name = nameTextField.getText();
				String description = descriptionTextField.getText();
				if(name.equals(""))
					return null;
				if(description.equals(""))
					return null;
				
				VertexClass vertex = new VertexClass(0.0,0.0,Integer.toString(i++));
				Milestone milestone = milestones.getMilestoneByName((String)milestoneComboBox.getSelectedItem());
				Status status = new Task4Statuses();
				Task task = new ITTask(null, milestone.getProject(), milestone, name, status);
				task.setDescription(description);
				Database.saveOrUpdate(task);
				milestone.addTask(task);
				vertex.setTask(task);
				tasks.addTask(task);
				return vertex;
			}
			else
			{
				return null;
			}
		}
		else
		{
			return new VertexClass(0.0,0.0,Integer.toString(i++));
		}
	}
	
	public void reset()
	{
		this.i = 0;
	}
	
	public TaskContainer getTasks() {
		return tasks;
	}

	public void setTasks(TaskContainer tasks) {
		this.tasks = tasks;
	}

	public MilestoneContainer getMilestones() {
		return milestones;
	}

	public void setMilestones(MilestoneContainer milestones) {
		this.milestones = milestones;
	}

	public boolean turnedOn() {
		return trigger;
	}

	public void setTrigger(boolean trigger) {
		this.trigger = trigger;
	}
}
