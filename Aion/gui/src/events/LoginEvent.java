package events;
import java.util.EventObject;

import pojo.User;

public class LoginEvent extends EventObject {	

	private static final long serialVersionUID = 5588234880380459853L;
	private int step = 0;
	private User user;
	
	public LoginEvent(Object source, int step, User user) {
		this(source, step);
		this.user = user;
	}
	
	public LoginEvent(Object source, int step) {
		super(source);
		this.user = null;
		this.step = step;
	}
	
	public int getStep() {
		return step;
	}
	
	public User getUser() {
		return user;
	}
}
