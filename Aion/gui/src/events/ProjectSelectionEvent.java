package events;

import java.util.EventObject;

import pojo.Project;
import pojo.User;

public class ProjectSelectionEvent extends EventObject {
	private static final long serialVersionUID = -1338432444823633229L;
	private User user;
	private Project project;
	
	public ProjectSelectionEvent(Object source, User user, Project project) {
		super(source);
		this.user = user;
		this.project = project;
	}
	
	public User getUser() {
		return user;
	}

	public Project getProject() {
		return project;
	}
}
