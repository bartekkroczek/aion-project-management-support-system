package events;

import java.util.EventObject;

public class RegistrationEvent extends EventObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8598443872310629528L;
	private int step = 0;
	
	public RegistrationEvent(Object source, int step) {
		super(source);
		this.step = step;
	}
	
	public int getStep() {
		return step;
	}
}
