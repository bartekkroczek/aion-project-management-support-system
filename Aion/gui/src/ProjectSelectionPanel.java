import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.MouseInputAdapter;
import events.ProjectSelectionEvent;

import pojo.Database;
import pojo.ITMilestone;
import pojo.ITProject;
import pojo.Milestone;
import pojo.Project;
import pojo.ProjectLeader;

public class ProjectSelectionPanel extends JPanel {

	private static final long serialVersionUID = 7475121654813135154L;

	public PanelListenerManager<ProjectSelectionEvent> listenerManager = new PanelListenerManager<>();

	private JList projectList;
	private JScrollPane projectPane;
	private JList milestoneList;
	private JScrollPane milestonePane;

	private JButton selectProjectButton;
	private JButton deleteProjectButton;
	private JButton createProjectButton;

	private JButton deleteMilestoneButton;
	private JButton createMilestoneButton;

	private ProjectLeader leader = null;

	/**
	 * create ProjectSelectionPanel
	 */

	public ProjectSelectionPanel() {

		projectList = new JList();
		milestoneList = new JList();
		selectProjectButton = new JButton("Select Project");
		deleteProjectButton = new JButton("Delete Project");
		createProjectButton = new JButton("Create Project");
		deleteMilestoneButton = new JButton("Delete Milestone");
		createMilestoneButton = new JButton("Create Milestone");

		projectPane = new JScrollPane(projectList);
		milestonePane = new JScrollPane(milestoneList);

		projectList.addMouseListener(new MouseInputAdapter() {
			public void mousePressed(MouseEvent arg0) {
				JList list = (JList) arg0.getSource();
				int index = list.locationToIndex(arg0.getPoint());
				if (index >= 0) {
					String projectName = list.getModel().getElementAt(index)
							.toString();
					
					Project project = leader.getProjectByName(projectName);
					if(project != null)
						updateMilestones(project);
				}
			}
		});

		selectProjectButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Project project = getSelectedProject();
				
				if(project != null)
					listenerManager.fireEvent(new ProjectSelectionEvent(
							this, leader, project));
			}
		});

		createProjectButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (leader == null)
					return;

				JTextField projectName = new JTextField();

				final JComponent[] inputs = new JComponent[] {
						new JLabel("Project name: "), projectName };
				JOptionPane.showMessageDialog(null, inputs, "New Project",
						JOptionPane.PLAIN_MESSAGE);
				if(projectName.getText().isEmpty())
				{
					System.out.println("Could not create empty project");
					return;
				}
				
				ITProject project = new ITProject();
				ITMilestone defaultMilestone = new ITMilestone();
				project.setProjectLeader(leader);
				project.setProjectName(projectName.getText());
				defaultMilestone.setName("Default milestone");
				project.getMilestones().add(defaultMilestone);
				defaultMilestone.setProject(project);
				leader.getProjects().add(project);
				Database.saveOrUpdate(leader);
				// kaskadowe zapisywanie
				update();
			}
		});

		deleteProjectButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Project project = getSelectedProject();
				
				if(project != null) {
					Database.removeObject(project);
					leader.getProjects().remove(project);
					Database.saveOrUpdate(leader);
					update();
					
					if(leader.getProjects().size() == 0) {
						// czysci liste milestonow
						updateMilestones(null);
					}
				}
			}
		});

		createMilestoneButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (leader == null)
					return;

				Project project = getSelectedProject();
				
				if(project == null)
					return;
				
				JTextField milestoneName = new JTextField();
				JTextField milestoneDescription = new JTextField();

				final JComponent[] inputs = new JComponent[] {
						new JLabel("Milestone name: "), milestoneName,
						new JLabel("Milestone description: "), milestoneDescription
				};
				
				JOptionPane.showMessageDialog(null, inputs, "New Milestone",
						JOptionPane.PLAIN_MESSAGE);
				
				if(milestoneName.getText().isEmpty())
				{
					System.out.println("Cannot create milestone with empty name");
					return;
				}
				
				if(milestoneDescription.getText().isEmpty())
				{
					System.out.println("Cannot create milestone with empty description");
					return;
				}
				
				ITMilestone milestone = new ITMilestone();
				
				milestone.setName(milestoneName.getText());
				milestone.setDescription(milestoneDescription.getText());
				project.getMilestones().add(milestone);
				milestone.setProject(project);
				Database.saveOrUpdate(leader);
				// kaskadowe zapisywanie
				updateMilestones(project);
			}
		});

		deleteMilestoneButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String selectedMilestoneName = (String) milestoneList
						.getSelectedValue();
			
				String selectedProjectName = (String) projectList
						.getSelectedValue();
				
				if (selectedMilestoneName == null || selectedProjectName == null)
					return;
				
				Project project = leader.getProjectByName(selectedProjectName);
				
				if(project == null)
					return;
				
				if(project.getMilestones().size() == 1)
				{
					System.out.println("Must be at least 1 milestone");
					return;
				}
				
				Milestone milestone = project.getMilestoneByName(selectedMilestoneName);
				
				if(milestone != null) {
					try{
						Database.removeObject(milestone);
						project.getMilestones().remove(milestone);
						updateMilestones(project);
					} catch(Exception ex) {
						System.out.println("Cannot delete milestone, maybe it has task assigned");
					}
				}
			}
		});

		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();

		gc.weightx = 0.5;
		gc.weighty = 0.5;

		gc.gridx = 0;
		gc.gridy = 1;
		gc.gridwidth = 1;
		add(new JLabel("Projects"), gc);

		gc.gridx = 1;
		gc.gridy = 1;
		gc.gridwidth = 1;
		add(new JLabel("Milestones"), gc);

		gc.weighty = 5;
		gc.weightx = 1;

		gc.gridx = 0;
		gc.gridy = 2;
		gc.gridwidth = 1;
		gc.gridheight = 5;
		gc.fill = GridBagConstraints.BOTH;
		add(projectPane, gc);

		gc.gridx = 1;
		gc.gridy = 2;
		gc.gridwidth = 1;
		gc.gridheight = 5;
		gc.fill = GridBagConstraints.BOTH;
		add(milestonePane, gc);

		gc.weightx = 0.1;
		gc.weighty = 0.1;

		gc.gridx = 2;
		gc.gridy = 2;
		gc.gridwidth = 1;
		gc.gridheight = 1;
		gc.fill = GridBagConstraints.VERTICAL;
		gc.anchor = GridBagConstraints.WEST;
		add(selectProjectButton, gc);

		gc.gridx = 2;
		gc.gridy = 3;
		gc.gridwidth = 1;
		gc.gridheight = 1;
		add(createProjectButton, gc);

		gc.gridx = 2;
		gc.gridy = 4;
		gc.gridwidth = 1;
		gc.gridheight = 1;
		add(deleteProjectButton, gc);

		gc.gridx = 2;
		gc.gridy = 5;
		gc.gridwidth = 1;
		gc.gridheight = 1;
		add(createMilestoneButton, gc);

		gc.gridx = 2;
		gc.gridy = 6;
		gc.gridwidth = 1;
		gc.gridheight = 1;
		add(deleteMilestoneButton, gc);

	}

	public ProjectLeader getLeader() {
		return leader;
	}

	public void setLeader(ProjectLeader leader) {
		this.leader = leader;
	}

	public void update() {
		updateProjects();
	}

	public void updateMilestones(Project project) {
		ArrayList<String> stringArrayList = new ArrayList<>();
		milestoneList.removeAll();
		
		if (leader != null && project != null) {
			for (Milestone milestone : project.getMilestones()) {
				stringArrayList.add(milestone.getName());
			}
		}

		milestoneList.setListData(stringArrayList.toArray());
	}

	public void updateProjects() {
		ArrayList<String> stringArrayList = new ArrayList<>();
		projectList.removeAll();

		if (leader != null) {
			for (Project project : leader.getProjects()) {
				stringArrayList.add(project.getProjectName());
			}
		}

		projectList.setListData(stringArrayList.toArray());
	}

	public void makeProject() {

	}
	
	public Project getSelectedProject() {
		if (leader == null)
			return null;

		Set<Project> projects = leader.getProjects();
		String selectedProjectName = (String) projectList
				.getSelectedValue();

		if (selectedProjectName == null)
			return null;

		Project project = leader.getProjectByName(selectedProjectName);
		return project;
	}
}
