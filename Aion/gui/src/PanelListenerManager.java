import java.util.EventObject;

import javax.swing.event.EventListenerList;


public class PanelListenerManager<EventType extends EventObject> {
	
	private EventListenerList eventListenerList = new EventListenerList();
	
	public void fireEvent(EventType event) {
		Object[] listeners = eventListenerList.getListenerList();
		
		for(int i = 0; i < listeners.length; i += 2) {
			if(listeners[i] == PanelListener.class) {
				((PanelListener)listeners[i+1]).eventOccured(event);
			}
		}
	}
	
	public void addListener(PanelListener listener) {
		eventListenerList.add(PanelListener.class, listener);
	}
	
	public void removeListener(PanelListener listener) {
		eventListenerList.remove(PanelListener.class, listener);
	}
}
