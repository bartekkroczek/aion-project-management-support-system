import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.mail.MessagingException;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.event.EventListenerList;

import org.hibernate.property.Getter;

import events.LoginEvent;
import pojo.Database;
import pojo.User;

import server.Mail;

public class LoginPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4932513626147661013L;

	public PanelListenerManager<LoginEvent> listenerManager = new PanelListenerManager<>();

	private JButton loginButton;
	private JButton registrationButton;
	private JButton forgotPasswordButton;
	private JButton changePasswordButton;
	private JTextField serverTextField;
	private JTextField portTextField;
	private JTextField loginTextField;
	private JPasswordField passwordField;
	private JPasswordField serverPasswordField;

	private static int TEXT_SIZE = 10;

	/**
	 * Create the panel.
	 */
	public LoginPanel() {
		loginButton = new JButton("Login");
		registrationButton = new JButton("Registration");
		forgotPasswordButton = new JButton("Forgot password");
		changePasswordButton = new JButton("Change password");
		serverTextField = new JTextField(TEXT_SIZE);
		portTextField = new JTextField(TEXT_SIZE);
		loginTextField = new JTextField(TEXT_SIZE);
		passwordField = new JPasswordField(TEXT_SIZE);
		serverPasswordField = new JPasswordField(TEXT_SIZE);

		loginButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				User user = null;
				try {
					if(loginTextField.getText().isEmpty())
					{
						System.out.println("login is empty");
						return;
					}
					
					if(passwordField.getText().isEmpty())
					{
						System.out.println("password is empty");
						return;
					}
					
					user = Database.getUser(loginTextField.getText(),
							passwordField.getText());
					System.out.println(loginTextField.getText()
							+ " login successful");
					listenerManager.fireEvent(new LoginEvent(this, 0, user));
				} catch (Exception ex) {
					System.out.println(ex.getMessage());
				}
			}
		});

		registrationButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				listenerManager.fireEvent(new LoginEvent(this, 1));
			}
		});

		forgotPasswordButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JTextField login = new JTextField();
				JTextField mail = new JTextField();

				final JComponent[] inputs = new JComponent[] {
						new JLabel("login: "), login, new JLabel("mail: "),
						mail };
				
				int option = JOptionPane.showConfirmDialog(null, inputs, "Password reset",
						JOptionPane.OK_CANCEL_OPTION);
				
				if(option == JOptionPane.CANCEL_OPTION)
					return;
				
				if (login.getText().isEmpty()) {
					System.out.println("empty login");
					return;
				}

				if (mail.getText().isEmpty()) {
					System.out.println("empty mail");
					return;
				}

				String newPassword = Long.toHexString(Double
						.doubleToLongBits(Math.random()));

				try {
					new Mail(mail.getText(), login.getText(), newPassword)
							.send();
					try {
						Database.resetPassword(login.getText(),
								mail.getText(), newPassword);
					} catch (Exception ex) {
						System.out.println("Failed to reset password");
					}
				} catch (MessagingException e1) {
					System.out.println("Failed to send e-mail");
				}

			}
		});

		changePasswordButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JTextField login = new JTextField();
				JPasswordField currentPassword = new JPasswordField();
				JPasswordField newPassword = new JPasswordField();

				final JComponent[] inputs = new JComponent[] {
						new JLabel("login: "), login, 
						new JLabel("current password: "), currentPassword,
						new JLabel("new password: "), newPassword
				};
				
				int option = JOptionPane.showConfirmDialog(null, inputs, "Password change",
						JOptionPane.OK_CANCEL_OPTION);

				if(option == JOptionPane.CANCEL_OPTION)
					return;
				
				if (login.getText().isEmpty()) {
					System.out.println("empty login");
					return;
				}

				if (currentPassword.getText().isEmpty()) {
					System.out.println("empty current password");
					return;
				}
				
				if (newPassword.getText().isEmpty()) {
					System.out.println("empty new password");
					return;
				}
				
				try {
				Database.changePassword(login.getText(), 
						currentPassword.getText(), newPassword.getText());
				} catch(Exception ex) {
					System.out.println("could not change password");
				}
			}
		});

		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();

		gc.weightx = 0.5;
		gc.weighty = 0.5;

		gc.gridx = 0;
		gc.gridy = 2;
		gc.gridwidth = 1;
		gc.fill = GridBagConstraints.NONE;
		add(new JLabel("Login"), gc);

		gc.gridx = 1;
		gc.gridy = 2;
		gc.gridwidth = 2;
		gc.fill = GridBagConstraints.HORIZONTAL;
		add(loginTextField, gc);

		gc.gridx = 0;
		gc.gridy = 3;
		gc.gridwidth = 1;
		gc.fill = GridBagConstraints.NONE;
		add(new JLabel("Password"), gc);

		gc.gridx = 1;
		gc.gridy = 3;
		gc.gridwidth = 2;
		gc.fill = GridBagConstraints.HORIZONTAL;
		add(passwordField, gc);

		gc.gridx = 0;
		gc.gridy = 5;
		gc.gridwidth = 1;
		gc.fill = GridBagConstraints.NONE;
		add(loginButton, gc);

		gc.gridx = 1;
		gc.gridy = 5;
		gc.fill = GridBagConstraints.NONE;
		add(registrationButton, gc);

		gc.gridx = 2;
		gc.gridy = 5;
		gc.fill = GridBagConstraints.NONE;
		add(forgotPasswordButton, gc);

		gc.gridx = 3;
		gc.gridy = 5;
		gc.fill = GridBagConstraints.NONE;
		add(changePasswordButton, gc);
	}
}
