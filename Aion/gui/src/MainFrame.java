import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.ConnectException;
import java.util.EventObject;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import org.hibernate.event.service.internal.EventListenerGroupImpl;

import pojo.Programmer;
import pojo.Project;
import pojo.ProjectLeader;
import pojo.User;
import server.FTPException;

import edu.uci.ics.jung.io.GraphIOException;
import events.LoginEvent;
import events.ProjectSelectionEvent;
import events.RegistrationEvent;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = -1616367561107980353L;
	private JPanel currentPanel;
	private LoginPanel loginPanel;
	private ProgrammerPanel programmerPanel;
	private LeaderPanel leaderPanel;
	private ProjectSelectionPanel projectSelectionPanel;
	private RegistrationPanel registrationPanel;

	public MainFrame() {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		setLayout(new BorderLayout());
		loginPanel = new LoginPanel();
		programmerPanel = new ProgrammerPanel();
		leaderPanel = new LeaderPanel();
		projectSelectionPanel = new ProjectSelectionPanel();
		registrationPanel = new RegistrationPanel();
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we)
	        {
	            if(leaderPanel.getGraphPanel().isEdited())
	            {
	            	Project project = leaderPanel.getProject();
	            	if(project == null)
	            		return;
	            	
	            	String filename = project.getId().toString();
	            	if(filename == null)
	            		return;
	            	
	            	try {
	            		leaderPanel.getGraphPanel().saveGraphToFile(filename);
	            	} catch(IOException e) {
	            		
	            	}
	            } 
	        }
		});

		loginPanel.listenerManager.addListener(new PanelListener() {
			
			@Override
			public void eventOccured(EventObject event) {
				// TODO Auto-generated method stub
				LoginEvent loginEvent = (LoginEvent)event;
				
				switch(loginEvent.getStep()) {
				case 0:
					// pressed login button
					User user = loginEvent.getUser();
					
					if(user instanceof ProjectLeader)
					{
						projectSelectionPanel.setLeader((ProjectLeader)loginEvent.getUser());
						projectSelectionPanel.update();
						changeCurrentPanel(projectSelectionPanel);
					} else if(user instanceof Programmer) {
						programmerPanel.setProgrammer((Programmer)user);
						programmerPanel.update();
						changeCurrentPanel(programmerPanel);
					}
					break;
				case 1:
					// pressed registration button
					changeCurrentPanel(registrationPanel);
					break;
				}
			}
		});
		
		registrationPanel.listenerManager.addListener(new PanelListener() {
			
			@Override
			public void eventOccured(EventObject event) {
				RegistrationEvent registrationEvent = (RegistrationEvent)event;
				switch(registrationEvent.getStep()) {
				case 0:
					// pressed register button
					changeCurrentPanel(loginPanel);
					break;
				case 1:
					// pressed cancel button
					changeCurrentPanel(loginPanel);
					break;
				}
			}
		});
		
		projectSelectionPanel.listenerManager.addListener(new PanelListener() {
			
			@Override
			public void eventOccured(EventObject event) {
				ProjectSelectionEvent e = (ProjectSelectionEvent)event;
				leaderPanel.setUser(e.getUser());
				leaderPanel.setProject(e.getProject());
				String filename = e.getProject().getId().toString();
				try {
					leaderPanel.getGraphPanel().loadGraphFromFile(filename);
				} catch(Exception ex) {
					System.out.println(ex.toString());
					if(ex instanceof ConnectException) {
						return;
					}
					
					if(ex instanceof FTPException) {
						return;
					}
				}
				leaderPanel.getGraphPanel().countPrep();
				leaderPanel.updateGraph();
				changeCurrentPanel(leaderPanel);
			}
		});

         changeCurrentPanel(loginPanel);
	}

	public void changeCurrentPanel(JPanel panel) {
		if (currentPanel != null)
			getContentPane().remove(currentPanel);
		currentPanel = panel;
		getContentPane().add(currentPanel, BorderLayout.CENTER);
		pack();
	}

	public static void main(String[] args) {
		MainFrame frame = new MainFrame();
		frame.setTitle("AION");
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
