package customer;

import java.io.*;
import java.net.*;
import javax.swing.*;
import java.awt.event.*;

import server.FTPException;
import server.Overwriting;

public class FTP {

	private JTextArea receivedMessage;
	private JTextField message;
	private DataInputStream reader;
	private DataOutputStream writer;
	private Socket socket;

	public FTP(String ip, int port) throws IOException {
		try {
			socket = new Socket(ip,port);
			reader = new DataInputStream(socket.getInputStream());
			writer = new DataOutputStream(socket.getOutputStream());

			System.out.println("network service ready for use");
		} catch (IOException ex) {
			throw ex;
		}
	}

	public void SendFile(String fileName, Overwriting over)
			throws Exception {
		writer.writeUTF("SEND");
		File file = new File(fileName);
		if (!file.exists()) {
			writer.writeUTF("File not found");
			writer.writeUTF("DISCONNECT");
			throw new FTPException("File Not Found Exception");
		}

		writer.writeUTF(fileName);
		String msgFromServer = reader.readUTF();
		if (msgFromServer.compareTo("File Already Exists") == 0) {
			if (over == Overwriting.allowed) {
				writer.writeUTF("Y");
			} else {
				writer.writeUTF("N");
				writer.writeUTF("DISCONNECT");
				throw new FTPException("File Already Exist Exception");
			}
		}
		FileInputStream fin = new FileInputStream(file);
		int ch;
		do {
			ch = fin.read();
			writer.writeUTF(String.valueOf(ch));
		} while (ch != -1);
		System.out.println("File sent successfully");
		writer.writeUTF("DISCONNECT");
		fin.close();
	
	}

	public void ReceiveFile(String fileName, Overwriting over) throws Exception {
		writer.writeUTF("GET");
		writer.writeUTF(fileName);
		String msgFromServer = reader.readUTF();

		if (msgFromServer.compareTo("File Not Found") == 0) {
			writer.writeUTF("DISCONNECT");
			throw new FTPException("File Not Found Exception");
		} else if (msgFromServer.compareTo("READY") == 0) {
			File file = new File(fileName);

			if (file.exists() && over == Overwriting.illegal) {
				writer.writeUTF("DISCONNECT");
				throw new FTPException("File Already Exist Exception");
			}

			FileOutputStream fout = new FileOutputStream(file);
			int ch;
			String temp;
			do {
				temp = reader.readUTF();
				ch = Integer.parseInt(temp);
				if (ch != -1) {
					fout.write(ch);
				}
			} while (ch != -1);
			fout.close();
			writer.writeUTF("DISCONNECT");
		}

	}
}