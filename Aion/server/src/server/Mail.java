package server;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.Properties;

public class Mail {

	private static final String HOST = "smtp.gmail.com";
	private static final int PORT = 465;
	private static final String FROM = "aionassistance@gmail.com";
	private static final String PASSWORD = "aionproject2013";
	private String SUBJECT;
	private String CONTENT;
	private String TO;

	public Mail(String email, String userName) {
		this.SUBJECT = "Thank you for registering!";
		this.TO = email;
		this.CONTENT = "<html> <body> <img src=\"http://s22.postimg.org/lmmld9l8x/line.png\""
				+ " width=\"430\" height=\"48\"/> <br> <br>"
				+ " Dear "
				+ userName
				+ ",<br> <br>  Thank you for registering on AION - Project Management Support System <br>"
				+ " platform.<br>We are very proud and happy that you decided to choose our solution."
				+ " <br> We believe that together we will fulfill many exciting projects!<br> "
				+ "Good luck<br> <br><br>The AION Support Team<br> <img src=\"http://s21.postimg.org/6rdsammdj/linia.png\""
				+ " width=\"430\" height=\"1\"/><br> <table> <tr> <td> <img src=\"http://s15.postimg.org/3vcohc0xj/minilogo.png\"/> "
				+ "</td> <td> <font size=\"1\">Aion Corporation. All rights reserved. <br>"
				+ "All trademarks are property of their respective owners in the US <br>and"
				+ " other countries. </font> </td> </tr> </table> </body> </html>";
	}

	public Mail(String email, String userName, String Password) {
		this.SUBJECT = "Aion - the password has been reset";
		this.TO = email;
		this.CONTENT = "<html> <body> <img src=\"http://s22.postimg.org/lmmld9l8x/line.png\" width=\"430\""
				+ " height=\"48\"/> <br> <br>"
				+ " Dear "
				+ userName
				+ ",<br> <br>  According to your request we now reset the password for"
				+ " your <br>AION Project account<br>Your new password:<br><h2>["
				+ Password
				+ "]</h2><br<br>We also recall that at any time you can change your password <br>in application "
				+ "settings panel. <br><br> Have a good day!<br> <br><br>The AION Support Team<br>"
				+ " <img src=\"http://s21.postimg.org/6rdsammdj/linia.png\" width=\"430\" height=\"1\"/><br>"
				+ " <table> <tr> <td> <img src=\"http://s15.postimg.org/3vcohc0xj/minilogo.png\"/> </td> <td>"
				+ " <font size=\"1\">Aion Corporation. All rights reserved. <br>All trademarks "
				+ "are property of their respective owners in the US <br>and other countries."
				+ " </font> </td> </tr> </table> </body> </html>";
	}

	public static void main(String[] args) {
		try {
			new Mail("bartek.kroczek@gmail.com", "Bartek").send();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public void send() throws MessagingException {
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtps");
		props.put("mail.smtps.auth", "true");
		Session mailSession = Session.getDefaultInstance(props, null);
		MimeMessage message = new MimeMessage(mailSession);
		message.setSubject(SUBJECT);
		MimeBodyPart textPart = new MimeBodyPart();
		textPart.setContent(CONTENT, "text/html; charset=ISO-8859-2");
		Multipart mp = new MimeMultipart();
		mp.addBodyPart(textPart);
		message.setContent(mp);
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(TO));
		Transport transport = mailSession.getTransport();
		transport.connect(HOST, PORT, FROM, PASSWORD);
		transport.sendMessage(message,
				message.getRecipients(Message.RecipientType.TO));
		transport.close();
	}
}
