package server;

import java.io.*;
import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

	private final int port = 5000;
	private ExecutorService executor;
	private ServerSocket serverSocket;
	private boolean flag;

	public Server() {
		try {
			serverSocket = new ServerSocket(port);
			executor = Executors.newCachedThreadPool();
			flag = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void work() throws IOException {
		while (flag) {
			try {
				Socket customerSocket = serverSocket.accept();
				executor.execute(new ClientRunnable(customerSocket));
				System.out.println("Nawiązano połączenie z: "
						+ customerSocket.getInetAddress().getHostAddress());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		executor.shutdown();
	}
	
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	

	public static void main(String[] args) {
		try {
			new Server().work();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}