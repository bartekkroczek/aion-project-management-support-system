package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.Socket;

public class ClientRunnable implements Runnable {

	Socket clientSocket;
	DataInputStream reader;
	DataOutputStream writer;
	boolean connected = true;

	public ClientRunnable(Socket socket) {
		try {
			clientSocket = socket;
			reader = new DataInputStream(clientSocket.getInputStream());
			writer = new DataOutputStream(clientSocket.getOutputStream());
			System.out.println("FTP Client Connected ...");
		} catch (Exception ex) {
		}
	}

	@Override
	public void run() {
		while (connected) {
			try {
				System.out.println("Waiting for Command ...");
				String Command = reader.readUTF();
				if (Command.compareTo("GET") == 0) {
					System.out.println("\tGET Command Received ...");
					SendFile();
					continue;
				} else if (Command.compareTo("SEND") == 0) {
					System.out.println("\tSEND Command Receiced ...");
					ReceiveFile();
					continue;
				} else if (Command.compareTo("DISCONNECT") == 0) {
					System.out.println("\tDisconnect Command Received ...");
					connected = false;
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	void SendFile() throws Exception {
		String filename = reader.readUTF();
		File file = new File(filename);
		if (!file.exists()) {
			writer.writeUTF("File Not Found");
			return;
		} else {
			writer.writeUTF("READY");
			FileInputStream fin = new FileInputStream(file);
			int c;
			do {
				c = fin.read();
				writer.writeUTF(String.valueOf(c));
			} while (c != -1);
			fin.close();
			writer.writeUTF("File Receive Successfully");
		}
	}

	void ReceiveFile() throws Exception {
		String filename = reader.readUTF();
		if (filename.compareTo("File not found") == 0) {
			return;
		}
		File file = new File(filename);
		String option;

		if (file.exists()) {
			writer.writeUTF("File Already Exists");
			option = reader.readUTF();
		} else {
			writer.writeUTF("SendFile");
			option = "Y";
		}

		if (option.compareTo("Y") == 0) {
			FileOutputStream fout = new FileOutputStream(file);
			int ch;
			String temp;
			do {
				temp = reader.readUTF();
				ch = Integer.parseInt(temp);
				if (ch != -1) {
					fout.write(ch);
				}
			} while (ch != -1);
			fout.close();
			writer.writeUTF("File Send Successfully");
		} else {
			return;
		}

	}
}
