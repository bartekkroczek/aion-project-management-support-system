package server;

@SuppressWarnings("serial")
public class FTPException extends Exception {

	private String detail;

	public FTPException(String a) {
		detail = a;
	}

	public String toString() {
		return detail;
	}
}
